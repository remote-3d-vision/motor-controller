#ifndef __LCD_H__
#define __LCD_H__

#include "Arduino.h"

typedef struct lcd_ctx_s
{
    HardwareSerial *serial;   
} lcd_ctx_t;

void lcd_set_serial(lcd_ctx_t *ctx, HardwareSerial *serial_target);
void lcd_clear(lcd_ctx_t *ctx);
void lcd_new_line(lcd_ctx_t *ctx);
void lcd_backlight(lcd_ctx_t *ctx, boolean on);
void lcd_print(lcd_ctx_t *ctx, char *message);
//void lcd_print_orientation(lcd_ctx_t *ctx, float pitch, float yaw);
void lcd_print_orientation(lcd_ctx_t *ctx, long pitch, long yaw);

#endif
