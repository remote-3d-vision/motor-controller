#include <string.h>

#include "lcd.h"

#define LCD_LINE_LENGTH 16

//Codes according to: http://learn.parallax.com/KickStart/27977
#define LCD_CLEAR 12
#define LCD_NEW_LINE 13
#define LCD_BACKLIGHT_ON 17
#define LCD_BACKLIGHT_OFF 18

void lcd_set_serial(lcd_ctx_t *ctx, HardwareSerial *serial_target)
{
    ctx->serial = serial_target;
    ctx->serial->begin(19200);
}

void lcd_clear(lcd_ctx_t *ctx)
{
    ctx->serial->write(LCD_CLEAR);
    delay(5);
}

void lcd_new_line(lcd_ctx_t *ctx)
{
    ctx->serial->write(LCD_NEW_LINE);
}

void lcd_backlight(lcd_ctx_t *ctx, boolean on)
{
    if (on)
        ctx->serial->write(LCD_BACKLIGHT_ON); 
    else
        ctx->serial->write(LCD_BACKLIGHT_OFF);
}

void lcd_print(lcd_ctx_t *ctx, char *message)
{
    ctx->serial->print(message);
}

//arduino can't sprintf floats
/*
void snprintf_float(char *dst, size_t len, float num)
{
    int before_decimal = int(num);
    int after_decimal = (int)((num - before_decimal)*1000); //3 decimal places
    snprintf(dst, len, "%03d.%03d", before_decimal, after_decimal);
}

void lcd_print_orientation(lcd_ctx_t *ctx, float pitch, float yaw)
{
    char pitch_str[16];
    char yaw_str[16];
    char pitch_float_str[8];
    char yaw_float_str[8];

    snprintf_float(pitch_float_str, sizeof(pitch_float_str), pitch);
    snprintf_float(yaw_float_str, sizeof(yaw_float_str), yaw);

    snprintf(pitch_str, LCD_LINE_LENGTH, "p: %s", pitch_float_str);
    snprintf(yaw_str, LCD_LINE_LENGTH, "y: %s", yaw_float_str);

    lcd_clear(ctx);
    lcd_print(ctx, pitch_str);
    lcd_new_line(ctx);
    lcd_print(ctx, yaw_str);
}
*/

void lcd_print_orientation(lcd_ctx_t *ctx, long pitch, long yaw)
{
    long pitch_integer = pitch / 1000;
    long pitch_fraction = pitch - (pitch_integer * 1000);
    long yaw_integer = yaw / 1000;
    long yaw_fraction = yaw - (yaw_integer * 1000);
    char pitch_str[16];
    char yaw_str[16];

    snprintf(pitch_str, LCD_LINE_LENGTH, "p: %03ld.%03ld", pitch_integer, pitch_fraction);
    snprintf(yaw_str, LCD_LINE_LENGTH, "y: %03ld.%03ld", yaw_integer, yaw_fraction);

    lcd_clear(ctx);
    lcd_print(ctx, pitch_str);
    lcd_new_line(ctx);
    lcd_print(ctx, yaw_str);
}
