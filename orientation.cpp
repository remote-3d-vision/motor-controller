#include <string.h>

#include "orientation.h"

void orientation_set_serial(orientation_ctx_t *ctx, HardwareSerial *serial_target)
{
    ctx->serial = serial_target;
    ctx->serial->begin(19200);
    ctx->read_state = 0;
}

/*
This function is designed to read a single byte off of serial each call
This is done to keep the read time consistent for smooth motor movement
The function executes in about 20uS regardless if there info on the serial
port or not.

Each orientation ranges between 0 and 359999 and thus only requires 3 bytes

We use '[' and ']' as framing bytes and in between are 6 bytes, 3 bytes
for each orientation, yaw and pitch. Yaw is first and pitch is second.

incoming_pitch and incoming_yaw are held in the orientation_ctx and
are updated with each read off of the serial 

When '[' is read, we set incoming_pitch/yaw to 0

The next 3 byte reads are mapped to the first 3 bytes of incoming_yaw
The next 3 byte reads are mapped to the first 3 bytes of incoming_pitch

When ']' is read, we update the pitch and yaw passed in by reference
*/
void orientation_read(orientation_ctx_t *ctx, long *pitch, long *yaw)
{
    if (ctx->serial->available() == 0)
        return;

    byte byte_read = ctx->serial->read();

    switch(ctx->read_state)
    {
        case 0:
            if ((char)byte_read == '[') 
            {
                ctx->read_state++;
                ctx->incoming_pitch = 0;
                ctx->incoming_yaw = 0;
            }
            break;
        case 1:
            *(byte *)&(ctx->incoming_yaw) = byte_read;
            ctx->read_state++;
            break; 
        case 2:
            *(((byte *)&(ctx->incoming_yaw)) + 1) = byte_read;
            ctx->read_state++;
            break;
        case 3:
            *(((byte *)&(ctx->incoming_yaw)) + 2) = byte_read;
            ctx->read_state++;
            break;
        case 4:
            *(byte *)&(ctx->incoming_pitch) = byte_read;
            ctx->read_state++;
            break; 
        case 5:
            *(((byte *)&(ctx->incoming_pitch)) + 1) = byte_read;
            ctx->read_state++;
            break;
        case 6:
            *(((byte *)&(ctx->incoming_pitch)) + 2) = byte_read;
            ctx->read_state++;
            break;
        case 7:
            if ((char)byte_read == ']')
            {
                *yaw = ctx->incoming_yaw;
                *pitch = ctx->incoming_pitch;
            }
            ctx->read_state = 0;
            break;
    }

    return;
}
