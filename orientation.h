#ifndef __ORIENTATION_H__
#define __ORIENTATION_H__

#include "Arduino.h"

typedef struct orientation_ctx_s
{
    HardwareSerial *serial;   
    int read_state;
    long incoming_pitch;
    long incoming_yaw;
}orientation_ctx_t;

void orientation_set_serial(orientation_ctx_t *ctx, HardwareSerial *serial_target);
void orientation_read(orientation_ctx_t *ctx, long *pitch, long *yaw); 

#endif
