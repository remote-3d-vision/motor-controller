/*
Serial Port Choices (Arduino Mega)

Port Name   Transmit Pin       Receive Pin
Serial          1(Also USB)        0 (Also USB)
Serial1         18                 19
Serial2         16                 17
Serial3         14                 15
*/
#include <string.h>

#include "lcd.h"
#include "orientation.h"

/*
    PORTA maps to pins 22-29 inclusive
    22 is LSB, 29 is MSB

    ex.
    PORTA &= 0xF0;
    would output HIGH on pins 29,28,27,26
    and LOW on pins 25,24,23,22

    We do this because digitalWrite is a slow method

    PORT TO DRIVER MAP
    
    YAW DRIVER:
    PORT 22 -> M0
    PORT 23 -> M1
    PORT 24 -> M2
    PORT 25 -> DIR

    PITCH DRIVER:
    PORT 26 -> M0
    PORT 27 -> M1
    PORT 28 -> M2
    PORT 29 -> DIR

    PORTC maps to pins 30-37 inclusive
    30 is MSB, 37 is LSB

    PORT TO DRIVER MAP

    YAW DRIVER:
    PORT 30 -> STEP 

    PITCH DRIVER:
    PORT 31 -> STEP

    CALIBRATION CONTROLS:
    PORT 32 -> CONFIGURATION / ORIENTATION TOGGLE
    PORT 33 -> 1 DEGREE / 10 DEGREE TOGGLE
    PORT 34 -> FORWARD / BACKWARD TOGGLE
    PORT 35 -> PITCH STEP
*/

#define YAW_STEP_1_OVER_1 0x00
#define YAW_STEP_1_OVER_2 0x01
#define YAW_STEP_1_OVER_4 0x02
#define YAW_STEP_1_OVER_8 0x03
#define YAW_STEP_1_OVER_16 0x04
#define YAW_STEP_1_OVER_32 0x05
#define YAW_DIR_CW 0x08
#define YAW_DIR_CCW 0x00

#define PITCH_STEP_1_OVER_1 0x00
#define PITCH_STEP_1_OVER_2 0x10
#define PITCH_STEP_1_OVER_4 0x20
#define PITCH_STEP_1_OVER_8 0x30
#define PITCH_STEP_1_OVER_16 0x40
#define PITCH_STEP_1_OVER_32 0x50
#define PITCH_DIR_CW 0x80
#define PITCH_DIR_CCW 0x00

//max pitch forward and back so the bracket doesnt hit the motor below
//full lean forward -> pitch is PITCH_MIN
//cameras facing straight ahead -> pitch is PITCH_LEVEL
//full lean back -> pitch is 2 * PITCH_MAX
//clockwise (cw) is in the forward leaning direction
#define PITCH_MAX 180000L
#define PITCH_MIN 0
#define PITCH_LEVEL PITCH_MAX / 2 

#define STEP_THRESHOLD_1_OVER_1 1799
#define STEP_THRESHOLD_1_OVER_2 899 * 128
#define STEP_THRESHOLD_1_OVER_4 449 * 64 
#define STEP_THRESHOLD_1_OVER_8 224 * 64
#define STEP_THRESHOLD_1_OVER_16 112 * 16
#define STEP_THRESHOLD_1_OVER_32 56 

#define YAW_STEP 0x80
#define PITCH_STEP 0x40

//time (in uS) between each step (1/2400Hz = 417)
#define TIME_BETWEEN_STEPS 417

//calibration
#define CALIBRATION_MODE 0x20
#define CALIBRATION_SPEED_10_DEGREE 0X10
#define CALIBRATION_FORWARD 0x08
#define CALIBRATION_PITCH_STEP 0x04

lcd_ctx_t lcd_ctx;
orientation_ctx_t orientation_ctx;

//current orientation
long pitch_current = 0;
long yaw_current = 0;

//target orientation
long pitch_target = 0;
long yaw_target = 0;

//used for timing the loop - each loop should take roughly the same amount
//of time per iteration so the number of pulses per second is roughly fixed
unsigned long loop_start_time;
unsigned long loop_end_time;
unsigned long loop_delta_time;

long pitch_delta;
long pitch_dir_mod;

//used for deciding whether to go clock-wise (cw) or counter-clock-wise(ccw)
long yaw_delta_cw;
long yaw_delta_ccw;
long yaw_delta_min;
long yaw_dir_mod;

//will be mapped to PORTA
byte step_res_dir;

//will be mapped to PORTC
byte do_step;

int calibration_button = 0;
int calibration_button_state = 0;
int calibration_button_last_state = 0;
long calibration_button_debounce_start_time;

void setup()
{
    lcd_set_serial(&lcd_ctx, &Serial3);
    orientation_set_serial(&orientation_ctx, &Serial);

    //set PORTA to output mode
    DDRA = 0xFF;

    //set PORTC to output on pins 30,31
    DDRC = 0xC0;
}

void loop()
{
    /*
    //LCD Debuggging - can only update 10 times per second
    delay(100);
    char line1[16];
    char line2[16];
    snprintf(line1, 16, "%s", something);
    snprintf(line2, 16, "%s", something);
    lcd_clear(&lcd_ctx);
    lcd_print(&lcd_ctx, line1);
    lcd_new_line(&lcd_ctx);
    lcd_print(&lcd_ctx, line2);
    */

    char line1[16];
    char line2[16];
    
    calibration_loop:

    if (!(PINC & CALIBRATION_MODE))
    {
        pitch_current = PITCH_LEVEL;
        //read off any buffered up orientation info
        while ( orientation_ctx.serial->read() != -1);
        goto orientation_loop;
    }

    //check if calibration push button was pressed / released
    calibration_button = (PINC & CALIBRATION_PITCH_STEP) ? 1 : 0;
    if (calibration_button != calibration_button_last_state)
    {
        calibration_button_debounce_start_time = millis();
    }
    
    //wait 20 ms debounce time 
    if ((millis() - calibration_button_debounce_start_time) > 20)
    {
        if (calibration_button != calibration_button_state)
        {
            calibration_button_state = calibration_button;

            //button is pressed
            if (calibration_button_state)
            {
                step_res_dir = 0x00;
                do_step = 0x00;
                do_step |= PITCH_STEP;

                if (PINC & CALIBRATION_FORWARD)
                {
                    step_res_dir |= PITCH_DIR_CW;
                }
                else
                {
                    step_res_dir |= PITCH_DIR_CCW;
                }

                step_res_dir |= PITCH_STEP_1_OVER_2;

                int step_count = 1;
                if (PINC & CALIBRATION_SPEED_10_DEGREE)
                {
                    step_count = 10;
                }

                PORTA = step_res_dir;

                while (step_count > 0) 
                {
                    PORTC |= do_step;
                    delayMicroseconds(50);
                    PORTC &= 0x00;
                    delayMicroseconds(50);
                    step_count--;
                }
            }
        }
    }

    calibration_button_last_state = calibration_button;

    goto calibration_loop;

    //using goto gets rid of normal loop overhead
    orientation_loop:

    //used to keep the overall loop time
    loop_start_time = micros();

    step_res_dir = 0x00;
    do_step = 0x00;
    
    //non-blocking - pitch/yaw are only updated once a complete orientation msg 
    //has been is read off the serial port (this will require multiple function calls)
    orientation_read(&orientation_ctx, &pitch_target, &yaw_target);

    //clamp the pitch
    if (pitch_target < PITCH_MIN)
    {
        pitch_target = PITCH_MIN;
    }
    else if (pitch_target > PITCH_MAX)
    {
        pitch_target = PITCH_MAX;
    }

    if (pitch_current > pitch_target)
    {
        //rotate counter clockwise - forward
        step_res_dir |= PITCH_DIR_CCW;
        pitch_dir_mod = -1;
        pitch_delta = pitch_current - pitch_target;
    }
    else
    {
        //rotate clockwise - backward
        step_res_dir |= PITCH_DIR_CW;
        pitch_dir_mod = 1;
        pitch_delta = pitch_target - pitch_current;
    }

    //get yaw direction and delta
    if (yaw_current > yaw_target)
    {
        yaw_delta_cw = (360000 - yaw_current) + yaw_target;
        yaw_delta_ccw = yaw_current - yaw_target;
    }
    else
    {
        yaw_delta_cw = yaw_target - yaw_current;
        yaw_delta_ccw = yaw_current + 360000 - yaw_target;
    }

    if (yaw_delta_cw < yaw_delta_ccw)
    {
        step_res_dir |= YAW_DIR_CCW;
        yaw_delta_min = yaw_delta_cw;        
        yaw_dir_mod = 1;
    }
    else
    {
        yaw_delta_min = yaw_delta_ccw;        
        step_res_dir |= YAW_DIR_CW;
        yaw_dir_mod = -1;
    }

    //assume pitch step
    do_step |= PITCH_STEP;

    //We are ignoring full and half steps for now for smoothness

    //if (pitch_delta > STEP_THRESHOLD_1_OVER_1)
    //{
    //    step_res_dir |= PITCH_STEP_1_OVER_1;
    //    pitch_current += 1800 * pitch_dir_mod;
    //}
    //else if (pitch_delta > STEP_THRESHOLD_1_OVER_2)
    //if (pitch_delta > STEP_THRESHOLD_1_OVER_2)
    //{
    //    step_res_dir |= PITCH_STEP_1_OVER_2;
    //    pitch_current += 900 * pitch_dir_mod;
    //}
    //else if (pitch_delta > STEP_THRESHOLD_1_OVER_4)
    //if (pitch_delta > STEP_THRESHOLD_1_OVER_4)
    //{
    //    step_res_dir |= PITCH_STEP_1_OVER_4;
    //    pitch_current += 450 * pitch_dir_mod;
    //}
    //else if (pitch_delta > STEP_THRESHOLD_1_OVER_8)
    if (pitch_delta > STEP_THRESHOLD_1_OVER_8)
    {
        step_res_dir |= PITCH_STEP_1_OVER_8;
        pitch_current += 225 * pitch_dir_mod;
    }
    else if (pitch_delta > STEP_THRESHOLD_1_OVER_16)
    {
        step_res_dir |= PITCH_STEP_1_OVER_16;
        pitch_current += 113 * pitch_dir_mod;
    }
    else if (pitch_delta > STEP_THRESHOLD_1_OVER_32)
    {
        step_res_dir |= PITCH_STEP_1_OVER_32;
        pitch_current += 56 * pitch_dir_mod;
    }
    else
    {
        do_step &= ~PITCH_STEP;
   }
    
    //assume yaw step
    do_step |= YAW_STEP;

    //We are ignoring full and half steps for now for smoothness

    //if (yaw_delta_min > STEP_THRESHOLD_1_OVER_1)
    //{
    //    step_res_dir |= YAW_STEP_1_OVER_1;
    //    yaw_current += 1800 * yaw_dir_mod;
    //}
    //else if (yaw_delta_min > STEP_THRESHOLD_1_OVER_2)
    //if (yaw_delta_min > STEP_THRESHOLD_1_OVER_2)
    //{
    //    step_res_dir |= YAW_STEP_1_OVER_2;
    //    yaw_current += 900 * yaw_dir_mod;
    //}
    //else if (yaw_delta_min > STEP_THRESHOLD_1_OVER_4)
    //if (yaw_delta_min > STEP_THRESHOLD_1_OVER_4)
    //{
    //    step_res_dir |= YAW_STEP_1_OVER_4;
    //    yaw_current += 450 * yaw_dir_mod;
    //}
    //else if (yaw_delta_min > STEP_THRESHOLD_1_OVER_8)
    if (yaw_delta_min > STEP_THRESHOLD_1_OVER_8)
    {
        step_res_dir |= YAW_STEP_1_OVER_8;
        yaw_current += 225 * yaw_dir_mod;
    }
    else if (yaw_delta_min > STEP_THRESHOLD_1_OVER_16)
    {
        step_res_dir |= YAW_STEP_1_OVER_16;
        yaw_current += 123 * yaw_dir_mod;
    }
    else if (yaw_delta_min > STEP_THRESHOLD_1_OVER_32)
    {
        step_res_dir |= YAW_STEP_1_OVER_32;
        yaw_current += 56 * yaw_dir_mod;
    }
    else
    {
        do_step &= ~YAW_STEP;
    }

    if (yaw_current > 359999)
    {
        yaw_current = yaw_current - 360000;
    }
    else if (yaw_current < 0)
    {
        yaw_current = yaw_current + 360000;
    }

    //set the step mode and direction for the 8 mode/direction pins
    PORTA = step_res_dir;
    //XXX these microsecond delays may be able to be lowered
    delayMicroseconds(50); 
    
    //pulse the step pins if they have been mapped to do so
    PORTC |= do_step;
    delayMicroseconds(50);
    PORTC &= 0x00;
    delayMicroseconds(50);

    //check if we need to go to calibration mode
    if ((PINC & CALIBRATION_MODE) > 0)
    {
        goto calibration_loop;
    }

    loop_end_time = micros();

    loop_delta_time = loop_end_time - loop_start_time;

    //pulse at a max of 2400 times per second (417uS ~= 1/2400Hz)
    if (loop_delta_time < TIME_BETWEEN_STEPS)
    {
        delayMicroseconds(TIME_BETWEEN_STEPS - loop_delta_time);
    }

    goto orientation_loop;
}
